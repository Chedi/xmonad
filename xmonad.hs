import XMonad
import System.IO
import Data.Ratio

import XMonad.Prompt
import XMonad.Prompt.Man
import XMonad.Prompt.Shell
import XMonad.Prompt.XMonad

import XMonad.Layout.Grid
import XMonad.Layout.Combo
import XMonad.Layout.Named
import XMonad.Layout.Circle
import XMonad.Layout.Spiral
import XMonad.Layout.Reflect
import XMonad.Layout.TwoPane
import XMonad.Layout.MosaicAlt
import XMonad.Layout.NoBorders
import XMonad.Layout.Fullscreen
import XMonad.Layout.PerWorkspace
import XMonad.Layout.LayoutModifier
import XMonad.Layout.WindowNavigation

import XMonad.Hooks.SetWMName
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers

import XMonad.Actions.CycleWS
import XMonad.Actions.SpawnOn
import XMonad.Actions.FloatKeys
import XMonad.Actions.CopyWindow
import XMonad.Actions.MouseGestures

import Graphics.X11.Xlib
import Graphics.X11.Xinerama
import Graphics.X11.ExtraTypes.XF86

import qualified Data.Map                         as M
import qualified XMonad.StackSet                  as W
import qualified XMonad.Actions.Search            as S
import qualified XMonad.Actions.Submap            as SM
import qualified XMonad.Actions.ConstrainedResize as Sqr

import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys, additionalKeysP, additionalMouseBindings)

basicLayout = Tall nmaster delta ratio where
    nmaster = 1
    ratio   = 1/2
    delta   = 3/100

basicDualLayout = Tall nmaster delta ratio where
    nmaster = 2
    ratio   = 1/2
    delta   = 3/100

vdualLayout   = named "vertical dual" $ avoidStruts basicDualLayout
hdualLayout   = named "horizontal dual" $ avoidStruts $ Mirror basicDualLayout
tallLayout    = named "tall" $ avoidStruts basicLayout
wideLayout    = named "wide" $ avoidStruts $ Mirror basicLayout
singleLayout  = named "single" $ avoidStruts $ noBorders Full
twoPaneLayout = named "two pane" $ TwoPane (2/100) (1/2)
mosaicLayout  = named "mosaic" $ MosaicAlt M.empty
gridLayout    = named "grid" Grid
fullscreen    = named "fullscreen" $ noBorders (fullscreenFull Full)
myLayoutHook  = smartBorders $ fullscreen   |||singleLayout   ||| wideLayout
                                            ||| twoPaneLayout ||| vdualLayout
                                            ||| hdualLayout   ||| mosaicLayout
                                            ||| gridLayout    ||| tallLayout

infixr 0 ~> -- <http://mauke.ath.cx/stuff/xmonad/xmonad.hs>
(~>) :: a -> b -> (a, b)
(~>) = (,)

myWorkspaces = [one, two, three, four, five]
myManageHook = composeAll
    [
      (role =? "gimp-toolbox" <||> role =? "gimp-image-window") --> (ask >>= doF . W.sink),
      resource =? "stalonetray" --> doIgnore
    ]
  where role = stringProperty "WM_WINDOW_ROLE"

searchEngineMap method = M.fromList [ ((0, xK_g), method S.google)
                                    , ((0, xK_c), method S.codesearch)
                                    , ((0, xK_w), method S.wikipedia)]

sspawn a x = spawn $ "flock -n ~/.xmonad/.lock." ++ a ++ " -c '" ++ x ++ "'"

main = do
    widths <- openDisplay "" >>= getScreenInfo >>= return . map rect_width
    let xmobarWidth = 9/10
        mainscreenW = fromIntegral $ head widths
        nbslots :: Int
        nbslots = round (mainscreenW * (1-xmobarWidth) / 16)

    xmproc <- spawnPipe "xmobar"
    xmonad $ docks $ ewmh def {
        modMask            = mod4Mask,
        terminal           = "terminator",
        workspaces         = myWorkspaces,
        layoutHook         = myLayoutHook,
        manageHook         = manageSpawn <+> myManageHook <+> manageHook def,
        startupHook        = setWMName "LG3D",
        borderWidth        = 1,
        handleEventHook    = handleEventHook def <+> XMonad.Hooks.EwmhDesktops.fullscreenEventHook,
        normalBorderColor  = black,
        focusedBorderColor = yellow,
        focusFollowsMouse  = False,
        logHook            = dynamicLogWithPP xmobarPP {
          ppOutput = hPutStrLn xmproc,
            ppTitle  = xmobarColor "green" "" . shorten 50}
       }`additionalKeys`
      [
       ((mod4Mask, xK_a                     ), spawn "emacs"                                  )
      ,((mod4Mask, xK_c                     ), spawn "clementine"                             )
      ,((mod4Mask, xK_f                     ), spawn "firefox"                                )
      ,((mod4Mask, xK_g                     ), spawn "google-chrome"                          )
      ,((mod4Mask, xK_e                     ), spawn "nautilus"                               )
      ,((mod4Mask, xK_o                     ), spawn "opera-stable"                           )
      ,((mod4Mask, xK_z                     ), spawn "pavucontrol"                            )
      ,((mod4Mask, xK_w                     ), spawn "qbittorrent"                            )
      ,((mod4Mask, xK_p                     ), spawn "dmenu_run -b"                           )
      ,((mod4Mask, xK_u                     ), sendMessage Shrink                             )
      ,((mod4Mask, xK_i                     ), sendMessage Expand                             )
      ,((mod4Mask, xK_b                     ), sendMessage ToggleStruts                       )
      ,((mod4Mask, xK_comma                 ), sendMessage (IncMasterN   1 )                  )
      ,((mod4Mask, xK_semicolon             ), sendMessage (IncMasterN (-1))                  )
      ,((mod4Mask .|. controlMask, xK_Up    ), nextScreen                                     )
      ,((mod4Mask .|. controlMask, xK_Down  ), prevScreen                                     )
      ,((mod4Mask .|. mod1Mask,    xK_Up    ), swapNextScreen                                 )
      ,((mod4Mask .|. mod1Mask,    xK_Down  ), swapPrevScreen                                 )
      ,((mod4Mask .|. shiftMask,   xK_Up    ), shiftNextScreen                                )
      ,((mod4Mask .|. shiftMask,   xK_Down  ), shiftPrevScreen                                )
      ,((mod4Mask .|. controlMask, xK_Right ), nextWS                                         )
      ,((mod4Mask .|. shiftMask,   xK_Right ), shiftToNext                                    )
      ,((mod4Mask .|. controlMask, xK_Left  ), prevWS                                         )
      ,((mod4Mask .|. shiftMask,   xK_Left  ), shiftToPrev                                    )
      ,((mod4Mask .|. shiftMask,   xK_a     ), withFocused $ keysResizeWindow (-10,  0) (1, 1))
      ,((mod4Mask .|. shiftMask,   xK_z     ), withFocused $ keysResizeWindow (0  ,-10) (1, 1))
      ,((mod4Mask .|. shiftMask,   xK_e     ), withFocused $ keysResizeWindow (10 ,  0) (1, 1))
      ,((mod4Mask .|. shiftMask,   xK_r     ), withFocused $ keysResizeWindow (0  , 20) (1, 1))
      ,((0, xF86XK_AudioLowerVolume         ), spawn "pactl set-sink-volume 0 -5%"            )
      ,((0, xF86XK_AudioRaiseVolume         ), spawn "pactl set-sink-volume 0 +5%"            )
      ,((0, xF86XK_AudioMute                ), spawn "pactl set-sink-mute 0 toggle"           )
      ,((0, xK_Print                        ), spawn "scrot"                                  )
      ]`additionalMouseBindings`
      [
        ((0, 9), \w -> nextWS)
       ,((0, 8), const $ spawn "xdotool key super+Tab")
      ]

one    = "web"
two    = "editor"
three  = "terminal"
four   = "misc"
five   = "files"

black  = "#000000"
yellow = "#FFCC00"
